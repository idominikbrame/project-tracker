import { Router } from 'express';


//set Controller Structure
interface Controller {
    pathName: string;
    router: Router;
}

export default Controller;