
//Create an interface representing a document in MongoDB
export interface user {
    firstName:string;
    lastName: string;
    email: string;
    password: string,
    dateJoined: Date,
    invoices: invoiceID[]
}

interface invoiceID {
    invoiceID: string}
}