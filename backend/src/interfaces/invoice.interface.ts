export interface invoice {
    title:string;
    customer: string;
    customerNumber: string;
    customerEmail: string,
    description: string
    amount: Number,
    dateCreated: Date,
    dateOwed: Date
}