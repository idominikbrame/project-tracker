import mongoose, { Schema, model, connect } from 'mongoose';
import {user} from '../interfaces/user.interface'

//Create a Schema corresponding to the document interface
const userSchema = new Schema<user>({
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, required: true},
    dateJoined: {type: Date, required: false, default: () => Date.now()},
    invoices:{type: [], required: false}
})

//Create a Model
export const UserModal = model<user>('user', userSchema)