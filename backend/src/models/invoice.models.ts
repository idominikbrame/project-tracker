import { Schema, model } from 'mongoose';
import {invoice} from '../interfaces/invoice.interface'

//Create a Schema corresponding to the document interface
const invoiceSchema = new Schema<invoice>({
    title: {type: String, required: true},
    customer: {type: String, required: true},
    customerNumber: {type: String, required: true},
    customerEmail: {type: String, required: true},
    description: {type: String, required: false},
    amount: {type: Number, required: true},
    dateCreated: {type: Date, required: false, default: () => Date.now()},
    dateOwed: {type: Date, required: false}
})


//Create a Model
export const InvoiceModal = model<invoice>('invoice', invoiceSchema)