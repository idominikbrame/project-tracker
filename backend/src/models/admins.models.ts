import mongoose, { Schema, model, connect } from 'mongoose';

//Create an interface representing a document in MongoDB
interface admin {
    firstName:string;
    lastName: string;
    adminID: number;
    email: string;
}

//Create a Schema corresponding to the document interface
const adminSchema = new Schema<admin>({
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    adminID: {type: Number, required: true},
    email: {type: String, required: true}
})

//Create a Model
const Admin = model<admin>('user', adminSchema)