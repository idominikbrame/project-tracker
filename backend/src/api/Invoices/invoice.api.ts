import {Request, Response, Router} from "express";
import {check, validationResult} from "express-validator";
import {InvoiceModal} from "../../models/invoice.models";
import * as dotenv from "dotenv";


export class InvoiceApi {
    public router = Router()
    public apiName: string;
    public pathName: string;
    private checkForm: any[] = [
        check('email').isEmail(),
        check('password').isAlphanumeric()
    ]

    constructor(apiName: string, pathName: string) {
        this.apiName = apiName;
        this.pathName = pathName;
        dotenv.config();
        this.initRoutes()
    }

    private newInvoice(req: Request, res: Response) {

        const {title, customer, customerNumber, customerEmail, description, amount, dateOwed} = req.body

        // title: {type: String, required: true},
        // customer: {type: String, required: true},
        // customerNumber: {type: String, required: true},
        // customerEmail: {type: String, required: true},
        // description: {type: String, required: false},
        // amount: {type: Number, required: true},
        // dateCreated: {type: Date, required: false, default: () => Date.now()},
        // dateOwed: {type: Date, required: false}
    }


    private deleteInvoice(req: Request, res: Response) {
    }


    public initRoutes()
    {

        //Register User Init
        this.router.post(`${this.pathName}/newInvoice`, this.newInvoice)

        //Delete User Init
        this.router.delete(`${this.pathName}/deleteInvoice`, this.deleteInvoice)

        //Login User Init

    }
}