import express, {
    Request,
    Response,
    NextFunction,
    Router,
    response
} from "express";

export class AdminsApi {
    public router = express.Router()
    public apiName: string
    public pathName: string;
    constructor(apiName: string, pathName: string) {
        this.initRoutes()
        this.apiName = apiName
        this.pathName = pathName;
    }

    public initRoutes() {

        //Test Route
        this.router.get((`${this.pathName}/test`), (Request, Response)=>{
            console.log(response)
            console.log(this.pathName)
            response.json({message: "test"})
        })
    }
}