import {request, Request, Response, Router} from "express";
import {check, validationResult} from "express-validator";
import bcrypt from 'bcrypt';
import {UserModal} from "../../models/users.models";
import * as dotenv from "dotenv";
import {createTokens, validateToken} from "../../jwt";

//User Class
export class User {
    public router = Router()
    public apiName: string;
    public pathName: string;
    private checkForm: any[] = [
        check('email').isEmail(),
        check('password').isAlphanumeric()
    ]

    constructor(apiName: string, pathName: string) {
        this.apiName = apiName;
        this.pathName = pathName;
        dotenv.config();
        this.initRoutes()
    }

    //Encrypt Password
    private encryptPassword = async (password: string): Promise<string> => {
        let saltRounds = 10;
        return await bcrypt.hash(password, saltRounds)
    }


    //Register New User
    private registerUser = async (req: Request, res: Response): Promise<void> => {
        let valResult = validationResult(req)
        if (!valResult.isEmpty()) {
            res.status(200).json({
                msg: 'Invalid value',
            })
        } else {
            const {email} = req.body
            const user = await UserModal.findOne({email: email})
            if (!user) {
                let hashedPass = await this.encryptPassword(req.body.password)
                const user = new UserModal({
                    email: req.body.email,
                    password: hashedPass,
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                })
                await user.save()
                let token = createTokens(email)
                res.cookie("invoice-tracker-access-token", token, {
                    //Options WIP
                    httpOnly: true
                })
                console.log(token)
                res.status(202).json({msg: 'user registered successfully', data: user})
            } else {
                res.status(200).json({msg: 'Email address already in use. Please use another email'})
            }

        }


    }

    //Return User Auth if Token Available
    private verifyTokenLogin = async (req: Request, res: Response): Promise<void> => {
        //@ts-ignore
        let user = await UserModal.find({email: req.authenticated.user})
        if (user.length !== 0) {
            res.status(202).json({msg: 'Login Successful', data: user[0]})
        } else {
            res.status(202).json({msg: 'Username or Password Incorrect'})
        }
    }


    //Delete User
    private deleteUser = async (req: Request, res: Response): Promise<void> => {
        const {email} = req.body
        UserModal.findOneAndDelete({email: email})
            .then(deletedUser => {
                if (!deletedUser) {
                    res.status(203).json({msg: 'No user data found'})
                } else {
                    res.status(202).json({msg: 'user deleted'})

                }
            })
            .catch((err) => res.status(400).json({msg: err}));

    }

    //Login User
    private login = async (req: Request, res: Response) => {
        // @ts-ignore
        let valResult = validationResult(req)
        if (!valResult.isEmpty()) {
            res.status(203).json({
                msg: 'Invalid value',
            })
        } else {
            const {email, password} = req.body
            let user = await UserModal.find({email: email})

            if (user.length !== 0 && password) {
                bcrypt.compare(password, user[0].password).then(function (result) {
                    if (result === true) {
                        let token = createTokens(email)
                        res.cookie("invoice-tracker-access-token", token, {
                            // Options WIP
                            httpOnly: true
                        })
                        res.status(202).json({msg: 'Login Successful', data: user[0]})
                    } else {
                        res.status(202).json({msg: 'Username or Password Incorrect'})
                    }
                });
            } else {
                res.status(202).json({msg: 'Username or Password Incorrect'})
            }
        }
    }

    //APIs For User
    public initRoutes() {

        //Register User Init
        this.router.post(`${this.pathName}/registerUser`, this.checkForm, this.registerUser)

        //Verify token and login
        this.router.post(`${this.pathName}/token`, validateToken, this.verifyTokenLogin)

        //Delete User Init
        this.router.delete(`${this.pathName}/deleteUser`, this.deleteUser)

        //Login User Init
        this.router.post(`${this.pathName}/loginUser`, validateToken, this.checkForm, this.login)

        //test
        this.router.get(`${this.pathName}/test`, (req: Request, res: Response) => {
            console.log(this.pathName)
            res.json({message: "Hello World"})
        })

    }
}