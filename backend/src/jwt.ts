import * as process from "process";
import {Request, NextFunction, Response} from "express";
import {sign, verify} from "jsonwebtoken";

export const createTokens = (email:string) :string => {
    return sign({email: email}, process.env.JWT_TOKEN_SECRET)
}

export const validateToken = (req:Request, res:Response, next:NextFunction) => {
    console.log(req.cookies)
    const accessToken = req.cookies["invoice-tracker-access-token"];
    if(!accessToken) {
        // @ts-ignore
        req.authenticated = false;
        return next()
    }

    try{
        const validToken = verify(accessToken, process.env.JWT_TOKEN_SECRET)

        if(validToken){
            // @ts-ignore
            req.authenticated = {
                auth: true,
                //@ts-ignore
                user: validToken.email
            };
            return next()

        }
    }
    catch (err) {
        return res.status(400).json({error: err})
    }
}
