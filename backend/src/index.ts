import express from 'express';
import cors from 'cors';
import {User} from "./api/users/users.api";
import {InvoiceApi} from "./api/Invoices/invoice.api"
import Controller from "./interfaces/Controller";
import mongoose from "mongoose";
import {AdminsApi} from "./api/admins/admins.api";
import * as dotenv from "dotenv";
import * as process from "process";
import cookieParser from "cookie-parser";
import {invoice} from "./interfaces/invoice.interface";


class expressApp{


    public port: number;
    public app = express();

    constructor(name: String, controllers: Controller[], port: number) {
        this.port = port
        dotenv.config();
        this.initializeApp(controllers);
        this.connectToDatabase()
    }

    private initializeApp(controllers: Controller[]){
        this.initializeMiddleware();
        this.initializeControllers(controllers);
    }

    public startServer() {
        this.app.listen(this.port, () => {
            console.log(`Server listening on port ${this.port}`)
        })
    }

    private initializeMiddleware() {
        this.app.use(cors({
            origin: 'http://localhost:5173',
            credentials: true,
            exposedHeaders: ["set-cookie"]
        }))
        this.app.use(cookieParser())
        this.app.use(express.json({limit: "100mb"}));
        this.app.use(express.urlencoded({extended: true}));
        mongoose.connection.on('error', err => {
            console.error(err)
        })
    }

    private initializeControllers(controllers: Controller[]) {
        controllers.forEach((controller) => {
            this.app.use("/", controller.router);
        });
    }

    private async connectToDatabase(){
        await mongoose.connect(`${process.env.DB_NAME}`)
            .catch(error => console.error(error));
        if(mongoose.connection.readyState === 1){
            console.log("DB Connected")
        }
    }




}

//APIs
const userApi: User = new User('User API', '/api/user');
const adminApi: AdminsApi = new AdminsApi('Admin API', '/api/admin');
const invoiceApi: InvoiceApi = new InvoiceApi('Invoice API', 'api/invoice');


//Start App Server
const app = new expressApp('Express App', [userApi, adminApi, invoiceApi], 5005);
app.startServer();
