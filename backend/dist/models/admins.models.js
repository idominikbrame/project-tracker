"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
//Create a Schema corresponding to the document interface
const adminSchema = new mongoose_1.Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    adminID: { type: Number, required: true },
    email: { type: String, required: true }
});
//Create a Model
const Admin = (0, mongoose_1.model)('user', adminSchema);
//# sourceMappingURL=admins.models.js.map