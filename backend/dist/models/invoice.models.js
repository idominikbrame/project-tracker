"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvoiceModal = void 0;
const mongoose_1 = require("mongoose");
//Create a Schema corresponding to the document interface
const invoiceSchema = new mongoose_1.Schema({
    title: { type: String, required: true },
    customer: { type: String, required: true },
    customerNumber: { type: String, required: true },
    customerEmail: { type: String, required: true },
    description: { type: String, required: false },
    amount: { type: Number, required: true },
    dateCreated: { type: Date, required: false, default: () => Date.now() },
    dateOwed: { type: Date, required: false }
});
//Create a Model
exports.InvoiceModal = (0, mongoose_1.model)('invoice', invoiceSchema);
//# sourceMappingURL=invoice.models.js.map