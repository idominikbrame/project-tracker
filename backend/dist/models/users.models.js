"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModal = void 0;
const mongoose_1 = require("mongoose");
//Create a Schema corresponding to the document interface
const userSchema = new mongoose_1.Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    dateJoined: { type: Date, required: false, default: () => Date.now() }
});
//Create a Model
exports.UserModal = (0, mongoose_1.model)('user', userSchema);
//# sourceMappingURL=users.models.js.map