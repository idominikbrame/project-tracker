"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const users_api_1 = require("./api/users/users.api");
const invoice_api_1 = require("./api/Invoices/invoice.api");
const mongoose_1 = __importDefault(require("mongoose"));
const admins_api_1 = require("./api/admins/admins.api");
const dotenv = __importStar(require("dotenv"));
const process = __importStar(require("process"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
class expressApp {
    constructor(name, controllers, port) {
        this.app = (0, express_1.default)();
        this.port = port;
        dotenv.config();
        this.initializeApp(controllers);
        this.connectToDatabase();
    }
    initializeApp(controllers) {
        this.initializeMiddleware();
        this.initializeControllers(controllers);
    }
    startServer() {
        this.app.listen(this.port, () => {
            console.log(`Server listening on port ${this.port}`);
        });
    }
    initializeMiddleware() {
        this.app.use((0, cors_1.default)({
            origin: 'http://localhost:5173',
            credentials: true,
            exposedHeaders: ["set-cookie"]
        }));
        this.app.use((0, cookie_parser_1.default)());
        this.app.use(express_1.default.json({ limit: "100mb" }));
        this.app.use(express_1.default.urlencoded({ extended: true }));
        mongoose_1.default.connection.on('error', err => {
            console.error(err);
        });
    }
    initializeControllers(controllers) {
        controllers.forEach((controller) => {
            this.app.use("/", controller.router);
        });
    }
    connectToDatabase() {
        return __awaiter(this, void 0, void 0, function* () {
            yield mongoose_1.default.connect(`${process.env.DB_NAME}`)
                .catch(error => console.error(error));
            if (mongoose_1.default.connection.readyState === 1) {
                console.log("DB Connected");
            }
        });
    }
}
//APIs
const userApi = new users_api_1.User('User API', '/api/user');
const adminApi = new admins_api_1.AdminsApi('Admin API', '/api/admin');
const invoiceApi = new invoice_api_1.InvoiceApi('Invoice API', 'api/invoice');
//Start App Server
const app = new expressApp('Express App', [userApi, adminApi, invoiceApi], 5005);
app.startServer();
//# sourceMappingURL=index.js.map