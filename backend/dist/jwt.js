"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateToken = exports.createTokens = void 0;
const process = __importStar(require("process"));
const jsonwebtoken_1 = require("jsonwebtoken");
const createTokens = (email) => {
    return (0, jsonwebtoken_1.sign)({ email: email }, process.env.JWT_TOKEN_SECRET);
};
exports.createTokens = createTokens;
const validateToken = (req, res, next) => {
    console.log(req.cookies);
    const accessToken = req.cookies["invoice-tracker-access-token"];
    if (!accessToken) {
        // @ts-ignore
        req.authenticated = false;
        return next();
    }
    try {
        const validToken = (0, jsonwebtoken_1.verify)(accessToken, process.env.JWT_TOKEN_SECRET);
        if (validToken) {
            // @ts-ignore
            req.authenticated = {
                auth: true,
                //@ts-ignore
                user: validToken.email
            };
            return next();
        }
    }
    catch (err) {
        return res.status(400).json({ error: err });
    }
};
exports.validateToken = validateToken;
//# sourceMappingURL=jwt.js.map