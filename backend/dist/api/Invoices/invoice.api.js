"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvoiceApi = void 0;
const express_1 = require("express");
const express_validator_1 = require("express-validator");
const dotenv = __importStar(require("dotenv"));
class InvoiceApi {
    constructor(apiName, pathName) {
        this.router = (0, express_1.Router)();
        this.checkForm = [
            (0, express_validator_1.check)('email').isEmail(),
            (0, express_validator_1.check)('password').isAlphanumeric()
        ];
        this.apiName = apiName;
        this.pathName = pathName;
        dotenv.config();
        this.initRoutes();
    }
    newInvoice(req, res) {
        const { title, customer, customerNumber, customerEmail, description, amount, dateOwed } = req.body;
        // title: {type: String, required: true},
        // customer: {type: String, required: true},
        // customerNumber: {type: String, required: true},
        // customerEmail: {type: String, required: true},
        // description: {type: String, required: false},
        // amount: {type: Number, required: true},
        // dateCreated: {type: Date, required: false, default: () => Date.now()},
        // dateOwed: {type: Date, required: false}
    }
    deleteInvoice(req, res) {
    }
    initRoutes() {
        //Register User Init
        this.router.post(`${this.pathName}/newInvoice`, this.newInvoice);
        //Delete User Init
        this.router.delete(`${this.pathName}/deleteInvoice`, this.deleteInvoice);
        //Login User Init
    }
}
exports.InvoiceApi = InvoiceApi;
//# sourceMappingURL=invoice.api.js.map