"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const express_1 = require("express");
const express_validator_1 = require("express-validator");
const bcrypt_1 = __importDefault(require("bcrypt"));
const users_models_1 = require("../../models/users.models");
const dotenv = __importStar(require("dotenv"));
const jwt_1 = require("../../jwt");
//User Class
class User {
    constructor(apiName, pathName) {
        this.router = (0, express_1.Router)();
        this.checkForm = [
            (0, express_validator_1.check)('email').isEmail(),
            (0, express_validator_1.check)('password').isAlphanumeric()
        ];
        //Encrypt Password
        this.encryptPassword = (password) => __awaiter(this, void 0, void 0, function* () {
            let saltRounds = 10;
            return yield bcrypt_1.default.hash(password, saltRounds);
        });
        //Register New User
        this.registerUser = (req, res) => __awaiter(this, void 0, void 0, function* () {
            let valResult = (0, express_validator_1.validationResult)(req);
            if (!valResult.isEmpty()) {
                res.status(200).json({
                    msg: 'Invalid value',
                });
            }
            else {
                const { email } = req.body;
                const user = yield users_models_1.UserModal.findOne({ email: email });
                if (!user) {
                    let hashedPass = yield this.encryptPassword(req.body.password);
                    const user = new users_models_1.UserModal({
                        email: req.body.email,
                        password: hashedPass,
                        firstName: req.body.firstName,
                        lastName: req.body.lastName,
                    });
                    yield user.save();
                    let token = (0, jwt_1.createTokens)(email);
                    res.cookie("invoice-tracker-access-token", token, {
                        //Options WIP
                        httpOnly: true
                    });
                    console.log(token);
                    res.status(202).json({ msg: 'user registered successfully', data: user });
                }
                else {
                    res.status(200).json({ msg: 'Email address already in use. Please use another email' });
                }
            }
        });
        //Return User Auth if Token Available
        this.verifyTokenLogin = (req, res) => __awaiter(this, void 0, void 0, function* () {
            //@ts-ignore
            let user = yield users_models_1.UserModal.find({ email: req.authenticated.user });
            if (user.length !== 0) {
                res.status(202).json({ msg: 'Login Successful', data: user[0] });
            }
            else {
                res.status(202).json({ msg: 'Username or Password Incorrect' });
            }
        });
        //Delete User
        this.deleteUser = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { email } = req.body;
            users_models_1.UserModal.findOneAndDelete({ email: email })
                .then(deletedUser => {
                if (!deletedUser) {
                    res.status(203).json({ msg: 'No user data found' });
                }
                else {
                    res.status(202).json({ msg: 'user deleted' });
                }
            })
                .catch((err) => res.status(400).json({ msg: err }));
        });
        //Login User
        this.login = (req, res) => __awaiter(this, void 0, void 0, function* () {
            // @ts-ignore
            let valResult = (0, express_validator_1.validationResult)(req);
            if (!valResult.isEmpty()) {
                res.status(203).json({
                    msg: 'Invalid value',
                });
            }
            else {
                const { email, password } = req.body;
                let user = yield users_models_1.UserModal.find({ email: email });
                if (user.length !== 0 && password) {
                    bcrypt_1.default.compare(password, user[0].password).then(function (result) {
                        if (result === true) {
                            let token = (0, jwt_1.createTokens)(email);
                            res.cookie("invoice-tracker-access-token", token, {
                                // Options WIP
                                httpOnly: true
                            });
                            res.status(202).json({ msg: 'Login Successful', data: user[0] });
                        }
                        else {
                            res.status(202).json({ msg: 'Username or Password Incorrect' });
                        }
                    });
                }
                else {
                    res.status(202).json({ msg: 'Username or Password Incorrect' });
                }
            }
        });
        this.apiName = apiName;
        this.pathName = pathName;
        dotenv.config();
        this.initRoutes();
    }
    //APIs For User
    initRoutes() {
        //Register User Init
        this.router.post(`${this.pathName}/registerUser`, this.checkForm, this.registerUser);
        //Verify token and login
        this.router.post(`${this.pathName}/token`, jwt_1.validateToken, this.verifyTokenLogin);
        //Delete User Init
        this.router.delete(`${this.pathName}/deleteUser`, this.deleteUser);
        //Login User Init
        this.router.post(`${this.pathName}/loginUser`, jwt_1.validateToken, this.checkForm, this.login);
        //test
        this.router.get(`${this.pathName}/test`, (req, res) => {
            console.log(this.pathName);
            res.json({ message: "Hello World" });
        });
    }
}
exports.User = User;
//# sourceMappingURL=users.api.js.map