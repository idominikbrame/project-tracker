"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateUserForm = void 0;
const express_validator_1 = require("express-validator");
const validateUserForm = (req, res) => {
    let valResult = (0, express_validator_1.validationResult)(req);
    if (!valResult.isEmpty()) {
        return res.status(200).json({
            msg: 'Invalid value',
        });
    }
};
exports.validateUserForm = validateUserForm;
//# sourceMappingURL=auth.js.map