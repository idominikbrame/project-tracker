import * as http from "http";

class initializeServer{
    public port: number = 5005;

    constructor() {
        this.logTest()
    }

    public startServer() {
        const server = http.createServer(function (req, res) {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write('Hello World!');
            res.end();
        }).listen((5005), () => {
            console.log(`listening on port ${this.port}`)
            console.log(server.listening)
        });

    }

    public logTest(): void {
        console.log("hello")
    }
}

const app = new initializeServer()

app.startServer()
