import {createStore} from "vuex";
import router from "./routes";
import createPersistedState from "vuex-persistedstate"
import axios from "axios";


const store = createStore({

    plugins: [createPersistedState({
        storage: window.sessionStorage,
    })],
    state () {
        return {
            loggedIn: false,
            user: {
                firstName: null,
                lastName: null,
                dateJoined: null,
                email: null
            }
        }
    },
    mutations: {
        login(state){
            state.loggedIn = true
        },
        logout(state){
            state.loggedIn = false
            state.user = {
                firstName: null,
                lastName: null,
                dateJoined: null,
                email: null,
            }
        },
        initiateUser(state, payload){
            state.user.dateJoined = payload.dateJoined;
            state.user.firstName = payload.firstName;
            state.user.lastName = payload.lastName;
            state.user.email = payload.email;
        },
        initializeLoginState(state) {
            if(document.cookie.match(/^(.*;)?\s*invoice-tracker-access-token\s*=\s*[^;]+(.*)?$/)){
                state.loggedIn = true
                router.push('/')
            }
        }
    },
    getters: {

    }
})

export default store;