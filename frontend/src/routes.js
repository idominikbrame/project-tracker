import Home from "./views/Dashboard.vue";
import Login from "./views/Login.vue";
import NewInvoice from "./views/NewInvoice.vue";
import CatchAll from "./views/CatchAll.vue"
import InvoiceView from "./views/InvoiceView.vue"
import {createRouter, createWebHistory} from "vue-router";
import Dashboard from "./views/Dashboard.vue";
import store from "./store";
import Account from "./views/Account.vue";

const routes = [
    {
        path: '/',
        name: 'Dashboard',
        component: Dashboard,
    },
    {
        path: '/account',
        name: 'Account',
        component: Account,
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
            hideNavbar: true,
        }
    },

    {
        path: '/newInvoice',
        name: 'New Invoice',
        component: NewInvoice},
    {
        path: "/invoice/:id",
        name: "Invoice",
        component: InvoiceView
    },{
        path: "/:pathMatch(.*)*",
        name: "notFound",
        component: CatchAll
    }
]

const router = createRouter({

    history: createWebHistory(),
    routes,
})

router.beforeEach(async (to, from) => {
    if (
        // make sure the user is authenticated
        !store.state.loggedIn &&
        // ❗️ Avoid an infinite redirect
        to.path !== '/login'
    ) {
        // redirect the user to the login page
        router.push('/login')
    }
})





export default router

