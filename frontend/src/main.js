import router from './routes'
import {createApp} from "vue";
import App from "../src/App.vue"
import store from './store'


// Create a new store instance.



createApp(App).use(router).use(store).mount('#app')
