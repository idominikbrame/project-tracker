# Project Tracker
Project Management Applications for general use
## Description:
This project will be used by associates and project managers to keep track 
of current tasks, bugs and things to do. It allows users to assign 
themselves to a task, mark it as complete and invite team members to assist 
with tasks and callaborate on brainstorming idea. It has a built-in chat 
feature where you can reference tasks and open them for a detailed view of 
what the project entails.

## Version History: 
- 0.0.1
    - Initiating Project

## Dependencies
(Windows/Mac/Linux w/ Modern Browser)
- NodeJS , Docker , MongoDB/Compass

## Tech Stack Being Used

- VueJS
- Mongoose
- KeyCloak
- ExpressJS/NodeJS

## Installing... 
- (pending instructions)
- 
## Executing...
- (pending instructions)

## Authors
Dominik Brame

##### <a href="https://www.linkedin.com/in/dominikbrame/"> Linkedin</a>